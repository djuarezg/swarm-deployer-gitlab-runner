__For privileged runners deployments, please refer to <https://gitlab.cern.ch/vcs/docker-machine-privileged-runners>__

__This repository contains the necessary objects required to start a set of gitlab-runners in a set of  Openstack nodes created with docker swarm .__

# Index
1. [Resources](#resources)
  * [Openstack project resources](#openstack-project-resources)
  * [Ceph resources (S3 Object Storage)](#ceph-resources-s3-object-storage)
1. [Prepare Openstack environment](#prepare-openstack-environment)
  * [Create the Keypair for the clusters](#create-keypair-for-the-clusters)
1. [Create secret in Openshift for cluster provisioning](#create-secret-in-openshift-for-cluster-provisioning)
1. [Deploy a cluster](#deploy-a-cluster)
1. [Deprovision a cluster](#deprovision-a-cluster)

# Resources
Included here are all the required resources for this deployment

## Openstack project resources
The Swarm clusters that this deployment will need should be created on this Openstack project, due to the IO requirements. If necessary, an increase of project quota can be requested by opening a SNOW ticket.

The required resources that will be used are:

* __Openstack Project__: `IT Gitlab CI`
* __Service account__: `runnersciadmin`
* __Openstack administration egroup__: ` runnersci-openstack-admins`

In order to be able to follow the complete procedure, the service account `runnersciadmin` needs to be member of ` runnersci-openstack-admins` egroup, which will be the administrator egroup for the Openstack Project.

In order to manage the Openstack project with your own account you need to be part of the ` runnersci-openstack-admins` egroup as well.

`runnersciadmin` credentials can be retrieved it from the `shared-runners-provisioning` configmap on either `gitlab` namespace on `dev` or `prod`.


## Ceph resources (S3 Object Storage)
Some runners have cache enabled, in order to ask for a new instance from the Ceph central service, please open a [SNOW ticket](https://cern.service-now.com/service-portal/report-ticket.do?name=general-help-rqf&fe=service-desk).

Here you can find the request that has been fulfilled: [RQF0989367](https://cern.service-now.com/service-portal/view-request.do?n=RQF0989367)

The provided credentials are needed in the following steps.

The endpoint must have a bucket named `swarmcache` and `swarmcachedev` as it will be used on the configuration. In order to make it easier, use https://s3browser.com/ to create the buckets.

# Prepare Openstack environment

Preparation tasks need to be done only once for all the runners.
In order to run __all__ the commands, please connect to `lxplus-cloud` as `runnersciadmin`: `ssh runnersciadmin@lxplus-cloud`.

To be able to work on the Openstack project an env variable needs to be changed:

```bash
export OS_PROJECT_NAME='IT Gitlab CI'
```

## Create the Keypair for the clusters
After logging in as `runnersciadmin`, if the keypair is inexistant, please create a new one:

```bash
#In order to check if it already exists:
openstack keypair list
#To create the Key pair:
openstack keypair create swarm-runners-keypair > ~/.ssh/swarm-runners-keypair
#Restrict the permissions of the keypair
chmod 400 ~/.ssh/swarm-runners-keypair
```
Keep in mind that the generated file will be used for subsequent operations.

# Create secret in Openshift for cluster provisioning

The playbook accepts a large number of parameters. They are defined in the following places:

* Global defaults are in [roles/init_vars/defaults/main.yml](roles/init_vars/defaults/main.yml)
* Defaults for a number of runner types are in [roles/init_vars/vars](roles/init_vars/vars)
* Some variables (mostly secrets) are required by [roles/init_vars/tasks/main.yml](roles/init_vars/tasks/main.yml)

To facilitate provisioning of runner clusters, for each GitLab instance (gitlab prod and gitlab-dev)
we create a ConfigMap `shared-runners-provisioning` in the corresponding Openshift cluster (i.e. the `gitlab` project in openshift.cern.ch and openshift-dev.cern.ch respectively).

This ConfigMap has a key for each runner type (runner types are also mapped to APB plans), with a JSON document as value.
THe JSON document provides all the required variables (secrets) and overrides of the default values for that type of runner
and that GitLab instance.

E.g. for gitlab-dev

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: shared-runners-provisioning
data:
  docker-build: |
    {
      "CLUSTER_NAME": "swarm-ci-runners-db-0-dev",
      "_apb_plan_id": "docker-build",
      "NODE_COUNT": 1,
      "SWARM_CLUSTER_KEYPAIR": "<contents of swarm-runners keypair created for Openstack>",
      "openstack_admin_user": "runnersciadmin",
      "openstack_admin_password": "<password of service account>",
      "ACCESS_KEY": "<S3 access key>",
      "SECRET_KEY": "<S3 secret key>",
      "REGISTRATION_TOKEN": "<shared runner registration token for gitlab-dev>",
      "CI_SERVER_URL": "https://gitlab-dev.cern.ch",
      "OPENSHIFT_URL": "openshift-dev.cern.ch",
      "OPENSHIFT_TOKEN": "<token of the gitlab-operations serviceaccount in gitlab project>"
    }
  docker-cvmfs: |
    {
      "CLUSTER_NAME": "swarm-ci-runners-0-dev",
      "_apb_plan_id": "docker-cvmfs",
      "NODE_COUNT": 2,
      "SWARM_CLUSTER_KEYPAIR": "<contents of swarm-runners keypair created for Openstack>",
      "openstack_admin_user": "runnersciadmin",
      "openstack_admin_password": "<password of service account>",
      "ACCESS_KEY": "<S3 access key>",
      "SECRET_KEY": "<S3 secret key>",
      "REGISTRATION_TOKEN": "<shared runner registration token for gitlab-dev>",
      "CI_SERVER_URL": "https://gitlab-dev.cern.ch",
      "OPENSHIFT_URL": "openshift-dev.cern.ch",
      "OPENSHIFT_TOKEN": "<token of the gitlab-operations serviceaccount in gitlab project>"
    }
```

# Deploy a cluster

Create a temporary directory to mount the project and build docker container locally:
```bash
git clone https://gitlab.cern.ch/vcs/swarm-deployer-gitlab-runner.git /tmp/swarm-deployer-gitlab-runner
cd /tmp/swarm-deployer-gitlab-runner
docker build --no-cache -t swarm-deployer-gitlab-runner .
```

Retrieve the deployment secrets:
```bash
# Login on the appropriate environment, depending on where to deploy the runners
oc login openshift.cern.ch
# OR
oc login openshift-dev.cern.ch

# Retrieve the deployment secrets depending on which type of runner to deploy
runner_type="docker-cvmfs" # or "docker-build"
oc export configmap/shared-runners-provisioning -n gitlab -o json | jq ".data[\"${runner_type}\"]" -r > /tmp/swarm-deployer-gitlab-runner/playbook-vars.json
```

Edit `/tmp/swarm-deployer-gitlab-runner/playbook-vars.json` and set `CLUSTER_NAME` to a unique value (`swarm-ci-runners-<somenumber>-dev`) - check existing
clusters in https://openstack.cern.ch/project/stacks/ (for the `IT Gitlab CI` project)

Run provision role using the local image:
```bash
docker run --rm -it --name swarm-deployer-gitlab-runner \
  swarm-deployer-gitlab-runner provision --timeout 60 --extra-vars "$(cat /tmp/swarm-deployer-gitlab-runner/playbook-vars.json)"
```

**Make sure to  `rm -rf /tmp/swarm-deployer-gitlab-runner` to remove secrets from local machine.**

# Deprovision a cluster

The same image and `playbook-vars.json` preparation steps from `Deployment` apply here.

Using the same cluster name on `playbook-vars.json` and running the following command waits for running jobs, unregisters the runners and deletes the cluster:
```bash
docker run --rm -it --name swarm-deployer-gitlab-runner \
  swarm-deployer-gitlab-runner deprovision --timeout 60 --extra-vars "$(cat /tmp/swarm-deployer-gitlab-runner/playbook-vars.json)"
```

# Reconfigure an existing cluster

Sometimes we want to apply changes to an existing cluster without recreating it from scratch. To do so, please use [the reconfiguration branch](https://gitlab.cern.ch/vcs/gitlab-ci/swarm-deployer-gitlab-runner/tree/reconfiguration). It will run the same tasks, except the cluster creation.

Please adjust accordingly to reconfigure as necessary, as this reconfiguration will stop current jobs.
